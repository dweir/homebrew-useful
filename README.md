# README #

This is my own personal [homebrew](http://brew.sh/) tap. It will
formulae for scientific libraries and utilities that are not provided
by mainstream homebrew taps. At the time of writing there are two
formulae and they're both dodgy:

* siloh5 - [Silo mesh and field library](https://wci.llnl.gov/simulation/computer-codes/silo) with HDF5 support

* xyscan - [xyscan is a data thief for
  scientists](https://rhig.physics.yale.edu/~ullrich/software/xyscan/)

As and when I run up against other obstacles, I'll include more!

Note that xyscan builds a GUI app. Homebrew doesn't like native GUI
apps in their own tap, but I think this is the cleanest, most orderly
way of building and installing it for myself. Once built I link
xyscan.app into my `$HOME/Applications` directory.

# Using #

To use this tap, run the following:
```
#!bash
$ brew tap dweir/useful https://dweir@bitbucket.org/dweir/homebrew-useful.git

```

This will add the tap to your homebrew installation.
