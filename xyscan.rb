class Xyscan < Formula
  desc "xyscan is a data thief for scientists"
  homepage "https://rhig.physics.yale.edu/~ullrich/software/xyscan/"
  url "https://rhig.physics.yale.edu/~ullrich/software/xyscan/Distributions/4.30/xyscan-4.30-src.tgz"
  sha256 "91a6e9c55bfd7ee9ad9fe4be9fb92e5de8f4aa47feb099b5b79423821283335c"

  depends_on "qt"
  
  def install
    system "qmake"
    system "make"
    system "cp", "-r", "docs", "xyscan.app/Contents/Resources/"
    system "cp", "license.txt", "gpl.txt", "xyscan.app/Contents/"
    prefix.install "xyscan.app"
  end

  def caveats
    <<~EOS
         Homebrew doesn't like GUI apps. You may want to link to xyscan.app,
         which has been installed to the below directory, manually.
    EOS
  end
end


